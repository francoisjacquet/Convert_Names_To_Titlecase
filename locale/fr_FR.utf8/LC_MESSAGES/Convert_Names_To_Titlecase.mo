��          <      \       p      q      y   U   �     �   	   �  )     b   0                   Convert Convert Names To Titlecase Convert user and student names to titlecase (first letter of each word to uppercase). Project-Id-Version: Convert Names To Titlecase plugin for RosarioSIS
Report-Msgid-Bugs-To: 
PO-Revision-Date: 2024-04-22 19:03+0200
Last-Translator: François Jacquet <info@rosariosis.org>
Language-Team: RosarioSIS <info@rosariosis.org>
Language: fr_FR
MIME-Version: 1.0
Content-Type: text/plain; charset=UTF-8
Content-Transfer-Encoding: 8bit
Plural-Forms: nplurals=2; plural=(n > 1);
X-Poedit-KeywordsList: ;dgettext:2
X-Poedit-Basepath: ../../..
X-Generator: Poedit 3.2.2
X-Poedit-SourceCharset: UTF-8
X-Poedit-SearchPath-0: .
 Convertir Convertir les noms première en majuscule Convertir les noms des utilisateurs et des élèves (première lettre de chaque mot en majuscule). 